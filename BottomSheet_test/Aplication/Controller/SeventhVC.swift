//
//  SeventhVC.swift
//  BottomSheet_test
//
//  Created by Владислав Мурыгин on 25.12.2020.
//

import UIKit

class SeventhVC: UIViewController {

    let buttonOne: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Получить данные", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 100 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(buttonOneAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Кейс 7"
        view.addSubview(buttonOne)
        
        let backButton = UIButton(type: .custom)
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal)
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)

    }
    
    @objc func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @objc func buttonOneAction(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.sheetViewController?.resize(to: .fixed(300))
            self.view.backgroundColor = .green
            self.buttonOne.isEnabled = false
            self.buttonOne.setTitle("+", for: .normal)
        })
    }
}
