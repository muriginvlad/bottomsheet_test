//
//  FifthVC.swift
//  BottomSheet_test
//
//  Created by Владислав Мурыгин on 23.12.2020.
//

import UIKit

class FifthVC: UIViewController {
    
    let textFiled: UITextField = {
        let screen = UIScreen.main.bounds
        let textFiled =  UITextField(frame: CGRect(x: 20, y: 100, width: screen.width - 40 , height: 40))
        textFiled.placeholder = "Enter text here"
        textFiled.font = UIFont.systemFont(ofSize: 15)
        textFiled.borderStyle = UITextField.BorderStyle.roundedRect
        textFiled.autocorrectionType = UITextAutocorrectionType.no
        textFiled.keyboardType = UIKeyboardType.default
        textFiled.returnKeyType = UIReturnKeyType.done
        textFiled.clearButtonMode = UITextField.ViewMode.whileEditing
        textFiled.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        return textFiled
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Кейс 5"
        
        textFiled.delegate = self
        self.view.addSubview(textFiled)
        
        let backButton = UIButton(type: .custom)
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal)
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5)) {
            self.textFiled.becomeFirstResponder()
        }
        
    }
    
    @objc func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
}


extension FifthVC: UITextFieldDelegate {
    
}
