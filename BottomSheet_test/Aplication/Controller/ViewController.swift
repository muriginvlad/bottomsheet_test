//
//  ViewController.swift
//  BottomSheet_test
//
//  Created by Владислав Мурыгин on 23.12.2020.
//

import UIKit



class ViewController: UIViewController {
    
    let simpleFirst: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 1", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 150 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleFirstAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    
    let simpleSecond: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 2", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 230 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleSecondAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let simpleThird: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 3", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 310 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleThirdAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let simpleFourt: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 4", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 390 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleFourtAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let simpleFifth: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 5", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 470 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleFifthAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    
    let simpleSixth: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 6", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 550 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleSixthAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let simpleSeventh: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 7", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 630 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleSeventhAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let simpleEighth: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 8", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 710 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleEighthAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(simpleFirst)
        view.addSubview(simpleSecond)
        view.addSubview(simpleThird)
        view.addSubview(simpleFourt)
        view.addSubview(simpleFifth)
        view.addSubview(simpleSixth)
        view.addSubview(simpleSeventh)
        view.addSubview(simpleEighth)
        
        title = "Примеры карточек"
        
        
    }
    
    @objc func simpleFirstAction(sender: UIButton!){
        let vc = FirstVC()

        let navContrl = UINavigationController(rootViewController: vc)
        
        
        var options = SheetOptions()
        options.useFullScreenMode = true
        options.shrinkPresentingViewController = false
        
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(250), .fullscreen], options: options)
        sheet.cornerRadius = 20
        
        present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleSecondAction(sender: UIButton!){
        let vc = SecondVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.useFullScreenMode = true
        options.pullBarHeight = 0
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(250), .fullscreen], options: options)
        sheet.cornerRadius = 20
        
        
        present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleThirdAction(sender: UIButton!){
        let vc = ThirdVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(250)], options: options)
        sheet.cornerRadius = 20
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleFourtAction(sender: UIButton!){
        let vc = FourtVC()
       let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(250)], options: options)
        sheet.cornerRadius = 20
        
        present(sheet, animated: true, completion: nil)
    }
    
    
    
    @objc func simpleFifthAction(sender: UIButton!){
        let vc = FifthVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.pullBarHeight = 0
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(200)], options: options)
        sheet.cornerRadius = 20
        sheet.autoAdjustToKeyboard = true
        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        
        present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleSixthAction(sender: UIButton!){
        
        let vc = SixthVC()
        let navContrl = UINavigationController(rootViewController: vc)
       
        var options = SheetOptions()
        options.pullBarHeight = 50
        options.shouldExtendBackground = false
        options.shrinkPresentingViewController = false

        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(200)], options: options)
        sheet.treatPullBarAsClear = true
        sheet.minimumSpaceAbovePullBar = 20
        sheet.cornerRadius = 30
        
        let testButton = buttonAboveSheetsSettings(viewPullBar: sheet.contentViewController.pullBarView)
        sheet.contentViewController.pullBarView.addSubview(testButton)
        sheet.contentViewController.gripColor = .none
        
        present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleSeventhAction(sender: UIButton!){
        let vc = SeventhVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(200),.fullscreen], options: options)
    
        present(sheet, animated: true, completion: nil)
    }
    
    @objc func simpleEighthAction(sender: UIButton!){
        let vc = EighthVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.shrinkPresentingViewController = false
        
        options.widthTransform = true
        options.widthTransformSize = 15
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(200),.fixed(500)], options: options)

        present(sheet, animated: true, completion: nil)
    }
    
    func buttonAboveSheetsSettings(viewPullBar: UIView ) -> UIButton {
        let screen = viewPullBar.bounds
        let testButton = UIButton()
        testButton.layer.cornerRadius = 10
        testButton.setTitle("Закрыть", for: .normal)
        testButton.frame = CGRect(x: screen.maxX - 100 , y: 10 , width: 80 , height: 30)
        testButton.backgroundColor = .red
        testButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return testButton
    }
    
    @objc func closeAction(sender: UIButton!){
        dismiss(animated: true, completion: nil)
    }
}


